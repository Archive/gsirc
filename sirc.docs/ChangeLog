ChangeLog for sirc and ssfe, by orabidoo <roger.espel.llima@pobox.com>

This ChangeLog is written from the beginning to the end, and not
the other way round.


Version 1.0

Put up sirc in 2 days because it was needed. First the basic handling
of server lines, then the user commands.  Stable by 1st Nov 94

Version 1.1 : 23rd Jan 95

Perl5 compatibility fix, and made it so that /w (or /whois) sends
the nick by default. Made it recognize all the $SIRC* environment
variables. Added /mo.

Version 1.2 : 6th Feb 95

Added a number of oper-only commands, checked them on an unconnected
server. Wrote a README. Added suport for &channels (local to the
server). Made it look at the directory it was executed in for the
bt_sockets.ph file. Fixed a bug in nickname handling that could make
the client lose your own nickname. Added online help. Sent out to
a few ftp sites.

Version 1.21 : 9th Feb 95

Complain on invalid help topics. Take nick and server on command-line,
and ignore extra arguments that are not options.

Version 1.21a : 20th Feb 95

Wrote ssfe; added -r switch to sirc to make it compatible with the
first versions of ssfe.

Version 1.3 : 9th Mar 95

Made it compatible with ssfe's protocol, making -r unnecessary. Gave
it the whole zer0-look. Added /query and recognized * as a short for
the current channel in a lot of places. Added tabkey and ^r, /clear,
/eval, ^t, prompts, command-line history. Read compressed help. Simple
substitution /alias-es. Added ^ for silent lines. /echo, .sircrc and
.sircrc.pl.

Version 1.4a : 11th Mar 95

Functions, hooks and userhosts. Wrote the programming docs and the
paragraph on ssfe, in README. Wrote zer0.pl and fixed a few bugs
in the hooks.

Version 1.4d : 12th Mar 95

Tabkey handler changed to keep track of the most recent entry properly.
Notify lists added, and the moment of evaluation of .sircrc fixed.
Patterns, -min and -max added to /list. Added /ignore. Made it so that
//command doesnt do aliases or functions. Fixed &getuserline.

Version 1.5 : 14th Mar 95

Rewrote and fixed a few things; finally added /server and caught 
disconnections with the proper messages. Fixed &getuserline some more. 
Removed /talkto and made /join do the work. 

Version 1.51 : 15th Mar 95

Ignored ^c in ssfe and made it interrupt a server connect() in sirc.
Made ssfe respond to SIGWINCH and redisplay the status/command lines.
Made it show modes and usermodes in the status line, and keep track
of them.

Version 1.6 : 16th Mar 95

Added /dcc chat, /dcc close chat, rewrote most of the main select loop. 
Fixed /dcc chat so it doesn't think it lost the connection when sent an 
empty line. Fixed some termcap handling in ssfe. Changed the convention 
to calling hooks not to include the hook type. Added the variable 
$add_ons. Added /cd and /system.

Version 1.61 : 17th Mar 95

Added the remaining dcc's: /dcc get, /dcc send, /dcc close get/send, 
and /dcc list. Added the hold mode to ssfe.

Version 1.99 : 18th Mar 95

Recognized the shell variable USERINFO. Changed (again) the way ssfe and 
sirc handle signals, so sirc alone can be stopped with ^c but sirc+ssfe 
can't. Renamed bt_sockets.ph to sircsock.ph. Made mksock recognize a 
bunch of architectures directly. Fixed /notify so it doesnt send a lot 
of 1-nick ISON's if a script does a lot of notify's at the same time. 
Made it work if you send an empty nick when you havent registered yet. 
Made ^x c exit the front-end, ^x i toggle ^b^v^_ handling. Added the 
undernet 2.9 specific commands. Called it all 1.99 (beta) and sent out 
for beta-testing.

Version 1.991 : 20th Mar 95

Bug fixes and additions: handled the numerics for /trace. Made it 
possible to /quit without being connected. Added the -beep option to 
ssfe, and ^x b to toggle it. Removed the -d (debug) option. Called
the dumb client dsirc and added the shell-script "sirc" itself.
Cut the main if/elsif/elsif structure in 2, as it would overflow some 
parsers. Fixed &getuserline in case it gets an EOF.

Version 1.992 : 21th Mar 95

Made the "install" script and removed a lot of installing instructions
from README. Added the "input" hook.

Version 1.993 : 22nd Mar 95

Reviewed the select/socket handling, minor changes, possibly fixed a
bug in /dcc send. Allowed /server while not connected.

Version 1.994 : 23rd Mar 95

Made ssfe compatible with older non-ansi C compilers *sigh*. Fixed
/dcc close chat for offered chats.

Version 1.995 : 26th Mar 95

Possibly fixed an annoying /dcc send bug. Fixed bug in zer0.pl's /ig
and /unig (they didn't work).

Version 1.9951

Fixed the chomping of :'s so it doesn't remove them from /away reasons.
Fixed a bug in SIGWICH handling and another in the prompts, in ssfe.

Version 1.9954 : 13th Apr 95

Added a 7-bit mode option (-7). Fixed /list to reset the max/min/pattern 
at the end of the list. Forget pending /dcc get if we couldn't write to 
the file. Print what server numerics come from if it's not the current 
one. Added the "dcc_request" hook. Fixed a bug in the back scrolling of 
the command-line (reported by Chag!nmj3e@darwin.clas.virginia.edu).

Version 1.9955 : 17 Apr 95

Fixed portability bug in Ultrix 4.3 for ssfe, reported by 
GG!choude@wolfe.eece.maine.edu. Fixed /motd and /admin so they don't 
show "from some.irc.server".

Version 1.9956 : 18 Apr 95

Fixed /join to a channel that we're already in, which didn't work.
Made ^o type the last msg (or dcc chat msg) you got on the command line.

Version 1.996 : 9 May 95

Fixed *the* /dcc bug and removed previous kludges to circumvent it 
(thanks to whoever made "strace", the best way to really see what's 
going on). /dcc send and get are no longer a big CPU hog. Showed 
"(away)" status in the status line. Handled ~/something and ~/something 
in /dcc send filenames. Added support for server passwords and ports 
specified in the format server.hostname:portnumber:password. 7-bit mode 
is now default, use -8 to start in 8-bit mode. Fixed the arguments to 
/kill. Made notify signoffs faster. Added logging with /log and 
/logfile. Made it so that ^x h and the like work when ssfe is waiting 
for a key in hold mode, so you can get out of hold mode even if there's 
a lot of coming text. Command-line history in ssfe redone to keep old 
lines when edited. Multiple offered DCC SEND requests from the same user 
don't get forgotten anymore. Re-did the &userhost system so it can 
handle more than one request at the time, and added the "notify_signon" 
and "notify_signoff" hooks.

Version 2.0 : 10th May 95

Accepted a filename in all the /dcc close's, and added /dcc rename.
Added full grouped and delayed auto-ops to zer0.pl.

Version 2.01 : 15 May 95

Switched to the GNU General Public License, corrected a couple glitches
in the PROGRAMMING docs, and made it upate the status bar when /join-ing
a channel in which we already are. Fixed ^y on Solaris.

Version 2.02 : 25th May 95

Made /leave and /part leave the current channel if none is specified,
so they become synonymous with /hop. Fixed a small bug in keeping track
of channel modes. Made it impossible to confuse ssfe with "ssfe escapes"
sent on irc, reported by T-Bone!tmorgan@mcs.csuhayward.edu.

Version 2.021 : 4th Jun 95

Made the empty command do like "/say", so you can do "/ /blah" as with
ircII.

Version 2.022 : 3rd Oct 95

Made auto-rejoin use the channel key if there was one; suggested by
slackie!agreb@pobox.com

Version 2.023 : 18th Oct 95

Added the REHASH command.

Version 2.03 : 23rd Oct 95

Made the idle times returned by /whois be translated into hours, minutes 
and seconds; made zer0.pl's /ub = /unban take an address as well as a 
nick. Made ops, deops and debans group 4 by 4 instead of 3 by 3. Changed 
the look of the status bar (suggested by slackie!agreb@pobox.com).
Added site-ignore, sitebans, sitekickbans, filterkickbans, and indexed 
ban removing (rewritten after some code by 
DayTrip!pyrex@sutr.novalink.com) to zer0.pl. Renamed zer0.pl to 
n0thing.pl, to avoid confusion with the ircII script, and arbitrarily 
started its version number at 2.0.

Version 2.04 : 26th Oct 95

Added support for the BSD interface to ttys on ssfe; sirc now compiles
properly on NeXT's, Sequent and other BSD machines that don't support
tcgetattr()/tcsetattr().

Version 2.041 : 3rd Nov 95

Made sirc not remove a leading : in a /quit reason, when showing it;
made hours, minutes and times in dates have 2 digits always. Possibly
made /dcc work better when the IP number is setup wrong on /etc/hosts,
or on multihomed systems.

Version 2.1 of n0thing.pl : 7th Nov 95

Added /setquit; made /ban take more than one nick.

Version 2.1 : 4th Dec 95

/aliases now take arguments like $0, $1- and $channel. /load now has a 
path, @loadpath, defaulting with $HOME/.sirc, the directory dsirc has 
been loaded from, and the current directory. It is now possible to
/msg =someone while not fully connected to a server.

Version 2.101 : 16th Dec 95

Added the possibility to pick a new nick when changing servers, with
/server newnick@server.name.

Version 2.102 : 23rd Jan 96

Added the -flow option to ssfe for terminals with ^S/^Q flow control.

Version 2.103 : 3rd Feb 96

Made /dcc work even if the system's own IP number as given by /etc/hosts 
is wrong (as happens often with dynamically assigned IP addresses).

Version 2.104 : 11th Mar 96

Added un-echoed password prompting to /oper, and a default port of 6667
for remote connects.

Version 2.11 : 22nd Mar 96

Made /dcc chat smarter: if we get a request from someone we've sent a 
request to, connect to theirs and forget ours.

Version 2.111 : 4th Apr 96

Fixed ssfe's handling of null chars, reported by flarp!tmorgan@pobox.com. 
Made the 'sirc' sh script only set $SIRCSERVER if $IRCSERVER is unset.

Version 2.12 : 9th Apr 96

Added the possibility to delete a timer with &deltimer; code contributed 
by Hevy!wally@totavia.com.

Version 2.122 : 20th Apr 96 

Made ssfe not redisplay the input line when ^a or ^e is pressed and the
corresponding cursor position is already visible.  

Version 2.123 : 12th May 96

Recognized .pl as the default extension for the /load command. Added
/log and /logfile to the /help.

Version 2.13 : 12th May 96

Fixed a very annoying bug in how ssfe handles a screen resize (reported 
by slackie!agreb@pobox.com).  Added a hook for server notices, 
appropriately called "server_notice" (suggested by 
Marsala!mars@loeffel.txdirect.net).

Version 2.131 : 14th May 96

Fixed the "the server says" message to not chop the first word of the
line, to show server protocol errors.

Version 2.132 : 17 May 96

Made sirc take the nick from the 001 numeric, so we don't lose track of 
the user's nick when it was too long or invalid.

Version 2.133 : 17 Sep 96

Added -L to specify an alternate .sircrc file (patch suggested by Doug
Morris, w.d.morris.jr@larc.nasa.gov).

Version 2.14 : 3 Nov 96

Made sirc not prompt for a server, the user will have to type /server
whatever instead; this way one doens't lose the ability to dcc chat
while not connected (suggested by slackie!agreb@pobox.com).  Removed
some useless stuff from socket handling.  Added the "disconnect" hook,
and /reconnect to n0thing.pl.

Version 2.141 : 11 Nov 96

Minor change to ssfe.c to deal with terminfo-based systems that are
missing the termcap 'cs' capability on vt/xterm terminals.

Version 2.15 : 6 Dec 96

Added /printchan and printing of number of bytes transmitted in a /dcc
chat (after ideas by slackie!agreb@pobox.com and scripts by
flarp!tmorgan@pobox.com).  Signal handling tweaks to make ^c interrupt
/server and /dcc connects on some Solaris versions.  Added minimal
support for +channels (recognizes them as channels).

Version 2.151 : 8 Dec 96

Added /printuh to print user@host on msgs, invites and notices (after an
idea by slackie!agreb@pobox.com and a script by flarp!tmorgan@pobox.com,
as usual).  CTCP replies now show who they were sent to if it's not you
(suggested by slackie).  Added a max recursion limit on &docommand
(suggested by benny!mike@ef.net)

Version 2.4 of n0thing.pl : 8 Dec 96

Unbuffered away file so msgs are there even if sirc crashes/gets killed;
/remop takes patterns; bans with ?'s are recognized.

Version 2.2 of sirc : 12 Jun 97

Used perl5's Socket module if available; unbuffered logs; /query and
/dcc work when not connected; /join w/o args joins the last invited
channel; ^o shows u@h if printuh is on; some cosmetic changes in ctcp;
new hooks: status, print, command, dcc_disconnect, chat_disconnect,
send_ctcp; added an API for sockets (&socket, &connect, &resolve,
&listen, &accept, &nextfh) and for select()ing on fh's (&addsel,
&remsel); fixed some dcc chat bugs; push "=nick" to the tab queue when a
dcc chat is established; added support for virtual hosts; made a /dcc
rchat (rename chat); added a /set command, moved lots of things into it;
an interface for scripts to add /set vars; settable level of ctcp
support with noflood; comments in .sircrc allowed (line beginning with
#); log into .gz pipes thru a gzip; added a restricted (secure) mode
with no access to files or shell or perl; -q also not load .sircrc.pl
unless -l (or -L for .sircrc) given; added &load; redid the buffering of
stdin to avoid bugs with large pastes; refuse to dcc chat yourself;
remove ^W = clear screen from ssfe; fixed ssfe bug (chopping 1 char at
the end of continued lines); made ssfe cut a word if it starts in the
1st half of the line; optimized &dostatus redraws; added /dis and
/disconnect; made dcc send a lot faster (~3x) by sending ahead of the
acks (/set sendahead); added /server 1; a system-wide sircrc and
sircrc.pl; added support for socks proxies via a loadable script;
changed the install script to be more precise about compiling ssfe, to
use different default servers, to recognize perl 5, and to optionally
install socks support

Version 2.5 of n0thing.pl : 12 Jun 1997

moved /a from dsirc to n0thing; /rdc for dcc rchat; made /unig better;
added /pj; (settable) not show repeated away messages; most of the
settings moved into /set variables and made aliases for the old
commands; saved messages in memory rather than a file; added /wall and
/set wallv from wallops.pl; re-join channels and re-umode on reconnect
(settable); opbegs now use ctcp op and take a password; save more things
including /set variables in /sve; removed ctcp pong replies

Version 2.21 of sirc : 30 Jul 1997

fixed a bug with -q/-Q, corrected a couple glitches in the install script, 
not strip spaces at the end of /help topics, fixed the ops in the status
line with nicks containing a dash, added /set ctrl_t to set the string
^t sends, and /set loadpath for /load's path (suggested by
flarp!tmorgan@pobox.com), made keyboard input possible in hold mode (the
key to make it scroll is TAB), fixed a bug in DCC (found by
slackie!agreb@pobox.com), fixed deltimer (found by agodfrey@cs.uct.ac.za),
and added manpages for sirc and ssfe.

Version 2.211 of sirc : 10 Mar 1998

stripped anything after ^G from channel names, to be compatible with IRCnet.
a proper fix will come with the next version...


