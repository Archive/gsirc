           sirc, by orabidoo <roger.espel.llima@pobox.com>
	   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



This is a simple (well, not so simple anymore *grin*) irc client,
written in perl and C.

It's divided in two parts, a dumb-mode (meaning, no full screen
interface) client, dsirc, that can be used stand-alone, programmed in
perl, and a separate split-screen front end in C, called ssfe.

The main point of running this client is that you can get a reasonable
ircII-like interface on an account with a properly installed perl
interpreter, with the perl client taking about 60k (and it's usable on
its own if you don't have access to a C compiler) and with the
interface, once compiled, taking just another 30 or 40k.  The two
together, while lacking the huge complexity of ircII, make a very usable
client.  And if you know perl, you'll find you can do quite complicated
and interesting things with sirc too :)



## Files:

README		- this one
README.socks	- information about using sirc with a socks proxy
ChangeLog	- the list of changes in different versions
PROGRAMMING	- documentation for programming within sirc (scripting)
install		- installation script - will setup everything for you
dsirc		- the dumb-mode client itself
n0thing.pl      - a script for sirc
sirc.help.gz	- sirc's help file.  sirc can read it in compressed (.gz)
		  format, so there is no need to gunzip it here
ssfe.c		- source code for the front end
socks.pl	- module with the socks proxy support
sirc.1		- man page for sirc
ssfe.1		- man page for ssfe


## Installation:

If you're reading this, we'll assume that you've already uncompressed
the sirc.tar.gz file, and de-tared it, and you have all of these files a
directory, and it is your current directory. 

Then just type ./install, and follow the instructions.  In any case, if
you don't know what to answer to something the installation script is
asking you, just press Enter to use the default answer.

Once your dsirc and ssfe are working, the files you need to run the
client are:

 dsirc		- the client
 n0thing.pl	- the irc script, if you want it
 sirc		- little shell script to start the client
 ssfe		- the front end, if you want it
 sircsock.ph    - if the install script created it
 getopts.pl	- if it had to create it
 socks.pl	- if you want the socks proxy support
 sirc.help.gz	- if you want the online help

This brings the client to around 150k with everything, or 100k with the
client, the front-end and the help, or 65k with the dumb-mode client and
the help, or 60k without the help.

If you have enough disk space, you're encouraged to keep everything,
including the doc files README and PROGRAMMING.

For the client to work properly, the files "sirc.help.gz" and 
"sircsock.ph" and "getopts.pl" (if you have them) must be in the same 
directory as the main "dsirc" file.

You never need to uncompress sirc.help.gz, as the client is smart enough
to read it compressed.


## Invoking the client:

To run sirc: sirc [<options>] [<nick> [<server>]]

Options are:

  -d		- dumb mode (don't use ssfe) : must be the first option
  -p <number>	- specify port number
  -i <name>	- specify IRCNAME
  -n <nick>	- specify nickname (sorta useless as an option, you can
  		                    specify that without the -n)
  -s <server>	- specify server (sorta useless as an option too)
  -l <filename> - specify file to be loaded instead of ~/.sircrc.pl
  -L <filename> - specify file to be loaded instead of ~/.sircrc
  -H		- specify virtual host IP to bind to
  -q		- don't load ~/.sircrc or ~/.sircrc.pl
  -Q		- don't load system sircrc or sircrc.pl
  -R		- run in restricted (secure) mode
  -8		- run in 8-bit mode (do not translate iso-latin-1 to ASCII)


Example:
  sirc -i "my ircname" blurfer anarchy.tamu.edu

To run dsirc directly without using the "sirc" shell-script:

  [ssfe] [ssfe options] [perl] dsirc [<options>] [<nick> [<server>]]

As usual, arguments between [] are optional.

In restricted mode, sirc does not allow any access to files; the commands
LOAD, EVAL, CD, SYSTEM, DCC GET, DCC SEND are disallowed, as well as
running a .sircrc.pl and logging.

You can specify options for ssfe's operation in the environment variable
SSFE, since it's not possible to mix options for dsirc and for ssfe in
the same command-line using the "sirc" script.

So if you want beeps on by default, you should add the line
"setenv SSFE '-beep'" to your .login, or the line
"SSFE='-beep' ; export SSFE" to your .profile.


In case dsirc (or whatever program you run in ssfe) gets stuck, remember 
the combination of keys that will cause ssfe to exit: ^x c (i.e. 
Control-X followed by C).  When running in dumb mode, sirc can be 
interrupted by pressing ^c (Control-C).



## Recognized shell variables:

SIRCSERVER, IRCSERVER	- server to use, optionally followed by a port
			  and a password, in the form server:port:pass; 
			  overriden by -s or the second command-line 
			  option; defaults to us.undernet.org

SIRCPORT, IRCPORT	- port to connect to if not specified in the
			  server, overriden by -p, defaults to 6667

SIRCNAME, IRCNAME	- information between ()'s in the /whois,
			  defaults to "sirc user"

SIRCNICK, IRCNICK	- nickname to use, defaults to your username

SIRCRC			- file to load instead of ~/.sircrc

SIRCRCPL		- file to load instead of ~/.sircrc.pl

IRCFINGER		- reply to CTCP FINGER, defaults to
			  "keep your fingers to yourself"

USERINFO		- reply to CTCP USERINFO, defaults to
			  "yep, i'm a user"

SSFE			- options for ssfe, if you're using it.  set it
			  to "-beep" to have beep on, or to "-hold" to 
			  have hold-mode on, or to "-prompt '> '" to 
			  have "> " as a prompt, or any combination of 
			  such options

			  this variable is read by the "sirc" script,
			  not by dsirc or ssfe itself

SIRCHOST, IRCHOST,	- which host IP to use, on machines with several
LOCALHOST		  IP addresses


## Using it:

sirc has been designed to look a lot like ircII, while staying as simple 
and small as possible.  A number of things which are usually defined as 
aliases with ircII are builtin with sirc, to make it more usable.

sirc is undernet-friendly, /silence, /map, and even /rping and /uping
are there.

sirc's screen presentation of messages, talk lines, server lines,
and sirc's own messages follows these conventions:

  <nick> blah blah blah	     <- person spoke on a channel
  <yournick> blah blah blah  <- you spoke on a channel (your nick will
				be underlined or colored)
  * nick someaction	     <- person did a /me
  * yournick someaction      <- you did a /me (your nick will be underlined
				or somehow colored)
  [nick] blah blah blah	     <- you got a private message (the nick will
				be in bold)
  >nick< blah blah blah	     <- you sent a private message (the nick
				will be in bold)
  =nick= blah blah blah	     <- you got a message through a /dcc chat
				(nick is in bold)
  |nick| blah blah blah	     <- you sent a message through a /dcc chat
				(nick is in bold)
  -nick- blah blah blah	     <- you got a notice (nick in bold)
  -> -nick- blah blah blah   <- you sent a notice (no bold :p)
  *> nick action	     <- you got a /describe (nick in bold)
  *-> nick someaction	     <- you sent a /describe (nick in bold)

  *** Something		     <- general line from the server or client
  *E* Something		     <- there's been an error somewhere
  *D* Something		     <- something about DCC
  *H* Something		     <- you're getting help with /HELP
  *(* Something		     <- you're being notified of someone's signoff
  *)* Something		     <- you're being notified of someone's signon
  *<* Something		     <- someone left the channel or signed off
  *>* Something		     <- someone joined the channel
  *N* Something		     <- someone changed nicks
  *+* Something		     <- mode change
  *I* Something		     <- invite-related lines
  *T* Something		     <- channel topic-related lines
  *L* Something		     <- logging-related lines
  *?* Something		     <- unknown nick or channel


Commands start with a / (no surprise here), and the standard set of
commands is here:

ADMIN, ALIAS, AWAY, BYE, CD, CLEAR, CONNECT, CTCP, DCC, DESCRIBE, DIE,
EVAL, EXIT, HELP, IGNORE, INFO, INVITE, JOIN, KICK, KILL, LEAVE, LINKS,
LIST, LOAD, LUSERS, MAP, ME, MODE, MOTD, MSG, NAMES, NICK, NOTE, NOTICE,
NOTIFY, OPER, QUERY, PART, PING, QUERY, QUIT, QUOTE, RPING, SAY, SERVER,
SET, SIGNOFF, SILENCE, SQUIT, STATS, TIME, TOPIC, TRACE, UPING,
USERHOST, USERS, VERSION, WALLOPS, WHO, WHOIS, WHOWAS.

For help on a command, inside sirc, type /help <name of the command>
(once again, without the <>'s).

Like with ircII, commands preceded with a ^ after the / will be silent, 
and commands preceded with an extra / will not expand aliases or do 
functions.

In addition to the standard IRC commands, the following commands have
been added to sirc, or made more user-friendly (most of these are
inspired from popular ircII scripts):

CL		    - short for CLEAR

D [#chnl] nk1 nk2.. - deops a number of people in your the specified
DEOP [#chnl] nicks    channel (default = the current), grouping mode
		      changes 3 by 3

DE nick action      - short for DESCRIBE

HOP                 - leaves your current channel

IG [-][pattern]	    - short for IGNORE

I nicks [#chnnel]   - short for INVITE
INV nicks [#chnnel]   with I or INV as well as INVITE, you can specify
		      more than one nick, and the channel defaults to your
		      current channel

J channel           - short for JOIN
                      with J as well as JOIN, the trailing # is added
		      if not given;  without arguments, it will join
		      the channel you were most recently invited to.

K [#channel] reason - short for KICK
		      with K as well as with KICK, the channel defaults
		      to your current channel

LL                  - same as WHO <#currentchannel>

M nick mesg         - short for MSG

MO modes            - short for MODE <#currentchannel>

N [-][nick...]	    - short for NOTIFY

NEXT		    - goes to the next channel, if you're on several

NO nick mesg        - short for NOTICE

O [#chnl] nk1 nk2.. - ops a number of people in the specified channel
OP [#chnl] nicks      (default = the current channel), grouping mode
		      changes 3 by 3

P nick|#channel     - short for PING

T [#chnnl] [topic]  - short for TOPIC
                      with T as well as with TOPIC, the channel defaults
		      to your current channel

UMODE               - short for MODE <nickname>

W [nick]            - short for WHOIS, defaults to your own nick

WI nick             - asks for WHOIS info on nick's server, which gives
                      idle time, but takes longer to reply than /W


Some commands are partly taken from ircII, but work in a different
way:

ALIAS [-]alias text - without arguments, shows the list of aliases;
		      with one argument, shows a given alias, or deletes
		      it the first character is a "-"
		      with two arguments, defines an alias

		      the main difference with ircII is that aliases
		      are not evaluated recursively, so something like
			/alias blah foobar
			/alias foobar ^msg orabidoo
		      will *not* msg orabidoo when you do /blah, because
		      evaluation is not recursive.

		      substitution of arguments is implemented, so $0 
		      expands to the first argument, $1 the second, and 
		      so on; putting a "-" after the number makes it 
		      expand to that argument and all the following, so 
		      $0- is all the arguments and $1- is all the
		      arguments but the first. $$ expands to a $, and
		      $some_variable_name expands to the contents of
		      that variable (see the file PROGRAMMING for the
		      list of sirc's variables).

		      these aliases are meant only as little time savers;
		      for anything more complicated, use functions.

SET [-]var value    - without arguments, shows the list of variables
		      with their values.  with one argument, shows the
		      given variable.  with a variable name prefixed with
		      a "-", unsets that variable.  with two arguments,
		      sets a variable to a value.

		      see the list of SET variables below.

DCC cmd nick [args] - deals with direct client connections; available
		      commands are:
		      	/dcc chat nickname
			/dcc rchat oldname newname   <- rename a dcc chat
			/dcc get nickname [file]
			/dcc send nickname file
			/dcc rename nickname [oldfilename] newfilename
			/dcc close chat nickname
			/dcc close get nickname [file]
			/dcc close send nickname [file]
			/dcc list
			/dcc		<- same as /dcc list

EVAL code           - evaluates perl code in sirc's context.  useful for
		      little calculations and checks, as well as for
		      checking that your functions work.  as it can be
		      used to mess with the client's internal variables,
		      use at your own risk.

LOAD filename       - loads a sirc script, which is actually a file of
		      perl code, into sirc's context.  tilde-expansion is
		      done on the filename, which is searched for in all
		      the directories specified in the perl variable
		      @loadpath, which initially contains ~/.sirc, the
		      directory dsirc is installed in, and the current
		      directory.

		      see the file about programming.

		      an example script is provided, n0thing.pl, which
		      adds message logging, an auto-op list,
		      auto-rejoin, commands to handle bans, and more.

		      to try it out, inside sirc type /load n0thing.pl

SYSTEM command	    - corresponds to ircII's "EXEC": executes an external
		      unix command.  however, unlike ircII's exec, all
		      the ordinary irc functions are suspended while the
		      program is executing.  this means that any /SYSTEM
		      lasting more than 1 or 2 minutes will get you
		      disconnected from IRC with a "ping timeout".

		      the program's output goes to the screen, and if it
		      reads from its standard input, your keyboard lines
		      are fed as input to the program.

		      in *NO* case should you /SYSTEM programs that use
		      a full-screen interface, like pine or tin.
		      use ^z (control-Z, which suspends sirc/ssfe) for this.

		      /SYSTEM is useful for commands that print
		      something and then exit immediately, such as
		      "ls", "date", "from", "uptime" ...

		      you can also use it to start programs such as "mail",
		      to read your mail from inside IRC, but if you stay
		      more than 1 or 2 minutes you're likely to get
		      disconnected from IRC when you're done with the mail.


List of SETtable variables:

CTCP off|none|noreply|noflood|on|all
		    - toggles the amount of CTCP support in the client.
		      "none" disables the processing of all CTCPs, even
		      CTCP action -- they will be shown like messages with
		      embedded ^A's.  "noreply" and "off" are synonymous,
		      and disable all automatic CTCP replies.  incoming
		      CTCPs are still processed, so you can take DCC CHATs
		      and see actions.  "noflood" and "on" are synonymous;
		      this is the default value and will auto-reply to
		      those CTCPs that ask for it, up to 2 every 10 seconds.
		      "all" will process and reply to all CTCPs.

		      note that even in "all" mode, stacked CTCPs (more than
		      one in a single message) are not recognized.  I have
		      yet to see those in actual use other than in floods.

CTRL_T text         - sets the text that the ^t key types; the default is
		      "/next", which switches to the next channel.

EIGHT_BIT on|off    - if set to off, accented iso-latin-1 characters are
		      translated into their non-accented equivalents.
		      otherwise they are passed, unmodified.

FINGER text	    - used for replying to CTCP FINGERs.

IRCNAME text        - used as the ircname on the next server connection.

LOADPATH path	    - Sets the path in which sirc looks for scripts to /load;
		      it is formatted as a list of pathnames separated by
                      colons, and ~'s are allowed.

LOCALHOST	    - sets which IP address to use, on machines with
		      more than one (virtual hosts).

LOG on|off	    - toggles logging on or off, or tells if it's on
                      or off.  logging is just like with ircII except
		      that there are no log levels; if it's on, all that
		      goes to the screen goes to the logfile.  this is
		      the same as the old /log command.  if you log
		      into a .gz file, sirc automatically calls gzip to
		      log in compressed form.

LOGFILE filename    - sets the logfile.  defaults to ~/sirc.log

PRINTUH off|none|some|all|on
		    - toggles printing the user@host on messages, notices,
		      invites and the like.  "off" and "none" are synonymous
		      and will only print user@host on JOINs.  "some" will
		      print them in private messages and ctcps and a few
		      other useful places.  "all" will print them everywhere.

PRINTCHAN on|off    - toggles printing the channel on public messages and
		      actions.  by default this is off, and the channel is
		      not printed when it is the current channel.

SENDAHEAD number    - sets the amount by which the DCC SEND code will send
		      ahead of what is acknowledged.  this is a hack to
		      speed up DCC transfers;  a value of 0 switches back
		      to the old, slow DCC (like ircII's).  the default
		      value (4096) is enough to speed it up quite a bit,
		      and something like 16386 will make it even faster.
		      setting it higher than what your kernel is willing
		      to buffer will result in your client blocking; sizes
		      of up to 16k should be quite safe.

USERINFO text	    - used for replying to CTCP USERINFOs.


Note that /LOADed scripts can add SET variables; in particular n0thing.pl
implements all its toggles like that.


## Config files:

sirc loads up to four config files on startup: the system-wide
sircrc.pl, your .sircrc.pl, the system-wide sircrc, and your .sircrc,
in that order.

System-wide config files are located in the same directory as dsirc.
If you use the install program, the "sirc" shell-script puts this
directory in the environment variable SIRCLIB before starting the perl
client

$SIRCLIB/sircrc and ~/.sircrc can contain commands that will be recognized
as if the user had typed them right at the moment of connecting to the
server.

Commands in a sircrc need not start with an extra /, but they can start
with a ^ if they are to be silent, or with two /'s if you don't want
alias expansion.  Lines starting with a # are ignored.

These files is sourced at the moment of connecting to the server, after
checking that the nick is not in use, so you can use "join", "notify"
and similar commands from it without problems.

Typical commands to put there would be in the vein of ^alias s msg someone

You can specify a different name for this file, with the option -L.


The files $SIRCLIB/sircrc.pl and ~/.sircrc.pl can contain perl code that
will be sourced during initialization, and can define new functions
programmed in perl, and install help for them, and also set hooks (the
equivalent of ircII's /on's) and &load other perl scripts.

This file is sourced before even connecting to the server.

You can specify a different file name, instead of ~/.sircrc.pl, starting
sirc with the -l option.  This can be useful to load bots.  See the file
about programming.


Sourcing ~/.sircrc and ~/.sircrc.pl will will be skipped if you start
sirc with the -q option, unless alternate names were given with the -l
or -L options.

Sourcing of the two system-wide config files can be skipped with the
option -Q.


## Functions, hooks, bots:

From /loaded scripts and .sircrc.pl, you can define new functions
and give their implementation in perl, and define hooks to be
triggered when certain conditions occur.  You need to know perl for
this, obviously.

See the file about programming, and the example script n0thing.pl.


## Using the split-screen front end (and having fun with it):

When using ssfe, you have a command-line of about 510 characters,
and a scrolling-region separated from it by a status bar, which
shows your nick, user modes, away status, channel, channel modes,
and query.

The command-line editor recognizes the following editing keys:

( ^key means Control-key )

  ^\		  - interrupt ssfe and whatever program it's running,
		    and exit back to the unix prompt
  ^a		  - go to the beginning of the line
  ^b, left arrow  - move left a letter
  ^c		  - interrupt: ignored by the front-end, can be used
  		    to interrupt connecting to a server, with sirc
  ^d		  - delete the character under the cursor
  ^e		  - go to the end of the line
  ^f, right arrow - move right a letter
  ^h, del	  - erase the previous character
  ^i, tab	  - go to next /msg in msg history
  ^j, ^m, enter   - send the line
  ^k		  - erase from the cursor to the end of the line
  ^l		  - redisplay the status bar and the command line
  ^n, down arrow  - go to the next line in command-line history
  ^o		  - with sirc, type the last msg you got on the command
  		    line
  ^p, up arrow	  - go to the previous line in command-line history
  ^t		  - with sirc, switch to the next channel you're on
  ^u		  - erase command-line
  ^v		  - insert the next character literally, even if it's
  		    a ^something
  ^x b		  - toggle beep on or off (off by default)
  ^x c		  - exit the front end, back to the unix prompt
  ^x y		  - set hold mode on
  ^x h		  - toggle hold mode
  ^x i		  - toggle irc-mode (^b^v^_ handling) on and off
  ^x n		  - set hold mode off
  ^y		  - yank the current line in the history without sending it
  ^z		  - suspend ssfe and sirc and go back to the unix
  		    prompt - you come back with 'fg'

Note that to do effects on irc (bold, inverse, underlined), you need to
type ^v^b to get a ^b character (since ^b would otherwise move your
cursor to the left), and ^v^v to get a ^v character.

If ssfe is in hold mode, it shows a (h) at the right end of the status
bar, and it will ask you to press a key after each full screen of text.
The little "h" turns into a "H" when ssfe is actually waiting for you to
press a key.  You can turn hold mode on and off with the ^x y and ^x n
commands, toggle it with ^x h, and you can start ssfe in hold mode with
the -hold command-line option.

By default, ssfe will consider beeps non-printable characters and will
display them as a G in inverse-video.  You toggle beeps on and off
with the ^x b keys, and you can start ssfe with the -beep command-line
option to have beeps on.

You can also use ssfe to run any other command-line based program than
sirc in it, by specifying the name of the program as ssfe's first
argument, instead of "sirc":

   ssfe [<options for ssfe>] <program> [<program's options>]

The options for ssfe can be:

  -raw		   - disable word-wrap, and handling of control characters
  -cooked	   - enable those, but not handle ^v^b^_
  -irc		   - word-wrap, control chars and ^v^b^_ all as in irc
  -hold		   - set hold mode
  -beep		   - let beeps through in irc/cooked mode
  -flow            - keeps ^S/^Q as special characters, for terminals using
                     ^S/^Q flow-control
  -print	   - print your own commands back on the scrolling window
  -prompt <prompt> - set a prompt in the command-line (max 8 chars)

The default mode is -cooked, but when the executed program is sirc, a
little protocol between sirc and ssfe tells ssfe to switch to irc mode.
This same protocol is used to pass information back and forth for the
tab-key handler, the status line, and so on.

The default prompt is none, or "> " if -print is specified.

Examples:

  ssfe -raw -print mail		# read your mail with an irc interface!
  ssfe -print -prompt '$ ' sh	# a shell with an irc interface!!


## What isn't there

.  windows (really really don't count on it)
.  multiple IRC connections
.  full redisplays and some sort of lastlog (don't count on it either)

Support for all of these is planned in sirc 3.0, which will require perl 5.
Don't ask me when that will actually exist (:


## Acknowledgments:

All the code used here is original (or reused from other programs I've
done, mainly from my old bot in perl).

Part of the help was inspired/lifted from ircII's help.
Lots of ideas about the interface were directly inspired from ircII.

More ideas have been suggested by users and beta-testers.

Some bits of the interface are inspired from various ircII scripts,
mainly my own zer0.

Thanks to the following beta testers for bug reports and suggestions:

slackie!agreb@pobox.com
flarp!tmorgan@pobox.com
mcj!mcj@acquiesce.org
Chag!nmj3e@darwin.clas.virginia.edu
nYtr0!gr1124@iutainfo.univ-lyon1.fr
GG!choude@wolfe.eece.maine.edu
THX-1138!fleuret@clipper.ens.fr


## License:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


If you make improvements to sirc, please send me the modifications 
(context diffs appreciated) and they might make it to the next release.

For bug reports, comments, questions, email roger.espel.llima@pobox.com

You can always find the latest version of sirc at the following URL:
http://www.eleves.ens.fr:8080/home/espel/sirc.html

