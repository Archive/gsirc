#!/usr/bin/perl
# Gtk Front End to Sirc
#use Gnome;
use Gtk;
use Gtk::Atoms;
use Gtk::Gdk;
use IPC::Open3;
use Symbol;
use Getopt::Long;

%statusbar_counters = ();

# We look in here before passing a command to sirc
%gsirc_commands = ("JOIN", \&new_channel);  
%gsirc_channels = (); # lookup a channel, get a text widget
$current_channel = "main";

################################################################
# command line options... aren't they fun
%opthash = ();
&GetOptions(\%opthash, "nick=s", "server=s", "sirc=s", "help", "h");
# get the nick
if (defined $opthash{"nick"}) { 
  $nick = $opthash{"nick"}; 
} 
else {
  $nick = "gsirc_user";
}
# get the server
if (defined $opthash{"server"}) { 
  $server = $opthash{"server"}; 
} 
else {
  $nick = "irc.efnet.org";
}
# get the path to dsirc
if (defined $opthash{"sirc"}) { 
  $sirc = $opthash{"sirc"}; 
} 
else {
	print "Hello\n";
  $sirc = "dsirc";
}
# display help text and quit
if (defined $opthash{"h"} || defined $opthash{"help"}) {
  print "This is the help text for gsirc.\n";
  print "--nick Nickname       (sets your nick)\n";
  print "--server servername   (sets server to connect to)\n";
  print "--sirc <path>/dsirc   (sets location of main dsirc executable)\n";
  print "--help or -h          (prints this message)\n";
  exit;
}

################################################################


################################################################
# start sirc and stuff...
$WTR = gensym();
$RDR = gensym();
$ERR = gensym();

$pid = open3($WTR, $RDR, $ERR, $sirc, $nick, $server);
print $pid." is the pid of sirc\n";
# hopefully that worked
################################################################


################################################################
# tell gtk that it exists, and parse a gtkrc
#init Gnome("gsirc");
init Gtk;
parse Gtk::Rc "gtkrc";
# end parsing
################################################################


################################################################
# create the main window
$window = new Gtk::Window 'toplevel';
$window->set_usize(500,300);
$window->set_title("GSirc: $nick\@$server");
$window->signal_connect("delete_event", \&quit_sirc, "is_main");
$window->signal_connect("destroy", \&quit_sirc, "is_main"); 
# window done
################################################################
$mainbox = new_pane("main");
$window->add($mainbox);

$callback_id = Gtk::Gdk->input_add( fileno($RDR), "read", \&get_sirc_data);

################################################################
# And away we go!
show_all $window;
$global_entry->grab_focus;
main Gtk;
# the end.
################################################################


################################################################
# quit progtram gracefully
sub quit_sirc {
	my($widget, $data) = @_;
	if ($data eq "is_main") {
	  print "quit_sirc entered\n";
	  print $WTR "/quit gsirc is alive again\n";
	  Gtk->main_quit();
	} else {
		print $WTR "/part $data\n";
		$gsirc_channels{$data} = undef;
		destroy $widget;
	}
}

sub new_pane {
	my ($channel) = @_;
	# Returns a vbox...do with it what you like 
	my $mainbox;
	my ($menubar, $menu, $file_item, $close_item, $quit_item);
	my ($entry, $statusbar, $counter, $table, $vscrollbar);
	my $separator; #, $text;

	################################################################
	# create evil menu
	$menubar = new Gtk::MenuBar;

	$menu = new Gtk::Menu;
	$file_item = new Gtk::MenuItem "File";
	$close_item = new Gtk::MenuItem "Close";
	$quit_item = new Gtk::MenuItem "Quit";

	$file_item->set_submenu($menu);
	$menu->add($quit_item);
	$menubar->add($file_item);


	$quit_item->signal_connect("activate",
          	                 sub {  
        	                     print $WTR "/quit gsirc is alive again\n";
      	                       Gtk->main_quit(); 
    	                       }
  	                        );
	# evil menu is done
	################################################################
	

	################################################################
	# create a couple spiffy boxen
	$mainbox = new Gtk::VBox(0,0);

	#$buttonbox = new Gtk::HBox(10,0);
	#$buttonbox->border_width(2);

	$mainbox->pack_start($menubar,0,0,0);
	#$mainbox->pack_start($buttonbox,0,0,0);
	# job's done
	################################################################

	################################################################
	# create an entry box
	$entry = new Gtk::Entry;

        $global_entry = $entry;

	$mainbox->pack_end($entry,0,0,0);

	$entry->signal_connect_after("key_press_event", \&press_in_entry_handler );                            

	# end button
	################################################################

	################################################################


	################################################################
	# create the elusive statusbar
	$counter = 1;
	$statusbar = new Gtk::Statusbar;
	$statusbar->signal_connect("text_popped", \&statusbar_popped);
	$statusbar_counters{$statusbar} = \$counter;
	
	#$button = new Gtk::Widget "Gtk::Button",
	#  -label => "push something",
	#  -visible => 1,
	#  -parent => $buttonbox,
	#  GtkObject::signal::clicked => [\&statusbar_push, $statusbar];

	#$button = new Gtk::Widget "Gtk::Button",
	#  -label => "pop",
	#  -visible => 1,
	#  -parent => $buttonbox,
	#  -signal::clicked => [\&statusbar_pop, $statusbar];

	#$button = new Gtk::Widget "Gtk::Button",
	#  -label => "test contexts",
	#  -visible => 1,
	#  -parent => $buttonbox,
	#  -signal::clicked => [\&statusbar_contexts, $statusbar];
	
	$separator = new Gtk::HSeparator;
	$mainbox->pack_start($separator,0,0,0);
	
	$mainbox->pack_end($statusbar,0,0,0);
	# statusbar enslaved
	################################################################
	
	################################################################
	# create a nifty table
	$table = new Gtk::Table(2,2,0);
	$table->set_row_spacing(0,2);
	$table->set_col_spacing(0,2);
	$mainbox->pack_start($table,1,1,0);
	# table done
	################################################################


	################################################################
	# create the text box
	$text = new Gtk::Text(undef, undef);
	freeze $text;
	$table->attach_defaults($text, 0,1,0,1);
#	realize $text;
	$text->insert(undef, "useless", undef, "Hi I am a Text Box\n");

	$entry->signal_connect("activate", \&insert_text, $entry);
	$entry->set_user_data($channel);
	thaw $text;
	$gsirc_channels{$channel} = $text;

	# end text box
	################################################################


	################################################################
	# create a couple scrollbars
	#$hscrollbar = new Gtk::HScrollbar($text->hadj);
	#$table->attach($hscrollbar, 0, 1,1,2,[-expand,-fill],[-fill],0,0);
	
	$vscrollbar = new Gtk::VScrollbar($text->vadj);
	$table->attach($vscrollbar, 1, 2,0,1,[-fill],[-expand,-fill],0,0);
	# end scrollbars
	################################################################
	$entry->grab_focus;
	
	return $mainbox;
}


sub press_in_entry_handler
{
	$event = $_[1];
	$key = %{$event}->{"keyval"};

	if($key == 0xFF09)
	{
		print "Tab was pressed\n";
	}
	else
	{
		#print "key = $key\n";
	}
	return 1;
}

sub statusbar_push {
  my($widget, $statusbar) = @_;
  my $countref = $statusbar_counters{$statusbar};
	$statusbar->push(1, "Something ".($$countref++));
}

sub statusbar_pop {
  my($widget, $statusbar) = @_;
  $statusbar->pop(1);
}

sub statusbar_popped {
  my($statusbar, $context_id, $text) = @_;
	my $countref = $statusbar_counters{$statusbar};
  if (!$statusbar->messages) {
    $$countref = 1;
  }
}

sub statusbar_contexts {
  my($button, $statusbar) = @_;
  foreach $string ("any context", 
                   "idle messages", 
                   "some text", 
                   "hit the mouse", 
                   "hit the mouse2") {
    print "Gtk::StatusBar: context = \"$string\", context_id=", 
    $statusbar->get_context_id($string),"\n";
  }
}

sub insert_text {
  my($widget, $entry) = @_;
  my $string = $entry->get_text();
  my ($command, $comm_args);
	my $func;
	if ($string =~ /^\/(\w+)\s+(.*)/) {
		$command = uc $1;
		$comm_args = $2;
	}
	#  $text->insert(undef, undef, undef, $string."\n");
	 
	if (defined($gsirc_commands{$command})) {
		$func = $gsirc_commands{$command};
		&$func($comm_args);
	} elsif ($string =~ /^\/.*/) { # command
	  print $WTR $string."\n";
	} else { # message
		my $channel = $entry->get_user_data();
		print $WTR "/msg $channel $string\n" 
	}
  $entry->set_text("");
}

sub parse_text {
  my ($string) = @_;
  $_=$string;
  my $text;
  my $statusbar = shift;
  if (/^\*\cBN/) {
    if (/You /)      {
      ($nick)=$string=~/(\S+)$/;
      $statusbar->push(1, "You are $nick");
    }
  }
  if (/^\~(\#\w+)\~\<(.*)\>(.*)/) {
    my $text = $gsirc_channels{$1};
    $text->insert(undef, "useless", undef, "<$2>$3\n");
    return;
  }
  $text = $gsirc_channels{$current_channel};
  $text->insert(undef, "useless", undef, $string);
}

  
sub get_sirc_data {
  my($source, $condidtion) = @_;
  $length = sysread $RDR, $string, 16384;
  &parse_text($string);
}

sub new_channel {
	my($args) = @_;
	my $channel;
	if ($args =~ /^(#\w+).*/) {
		$channel = $1;
	} else { return; }

	if (defined($gsirc_channels{$channel})) { return; }
	my $window = new Gtk::Window 'toplevel';
    $window->set_usize(500,300);    
	$window->set_title("$channel");
	$window->signal_connect("delete_event", \&quit_sirc, $channel);
	$window->signal_connect("destroy", \&quit_sirc, $channel);
	$window->add( scalar(new_pane($channel)) );
	print $WTR "/join $args\n"; # in case there's a key or something
	$current_channel = $channel;
	show_all $window;
}

